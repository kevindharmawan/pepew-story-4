from django.test import TestCase, Client

# Create your tests here.

class Story1Test(TestCase):
    
    def test_url_exists(self):
        response = Client().get('/story1/')
        self.assertEquals(response.status_code, 200)

    def test_template_exists(self):
        response = Client().get('/story1/')
        self.assertTemplateUsed(response, 'story1/index.html')

    def test_title_and_static_exists(self):
        response = Client().get('/story1/')
        html_response = response.content.decode('utf8')
        self.assertIn("Kevin Dharmawan - 1906398515", html_response)
        self.assertIn("picture.jpg", html_response)