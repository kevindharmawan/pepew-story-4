$("#keyword").keyup( function() {
    var input = $(this).val();
    console.log(input);

    $.ajax({
        url: '/story8/data?q=' + input,
        success: function(data) {
            var array_items = data.items;
            console.log(array_items);
            $("#search-result").empty();
            for (i = 0; i < array_items.length; i++) {
                var title = array_items[i].volumeInfo.title;
                var author = array_items[i].volumeInfo.authors;
                var image = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                $("#search-result").append("<tr><td>" + author + "</td><td>" + title + "</td><td><img src=" + image + "></td></tr>");
            }
        }
    });
});
