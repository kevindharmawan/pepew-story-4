from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from webdriver_manager.chrome import ChromeDriverManager

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

import json

# Create your tests here.
class Story8Test(TestCase):

    def test_index_url_exists(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)

    def test_index_template_exists(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/index.html')

    def test_index_content_exists(self):
        response = Client().get('/story8/')
        html_response = response.content.decode('utf8')
        self.assertIn("Enter Keyword", html_response)

    def test_success_when_not_added_before(self):
        response = Client().get('/story8/data/?q=intelligent_investor')
        jsonres = json.loads(response.content)

        assert jsonres['items']
        assert jsonres['items'][0]['volumeInfo']['title']

class SearchBoxTest(StaticLiveServerTestCase):

    def setUp(self):
        # self.display = Display(visible=0, size=(1000, 1200))
        # self.display.start()
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        
        self.selenium = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)
        super(SearchBoxTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SearchBoxTest, self).tearDown()
        # self.display.stop()
        return

    def test_connection(self):
        browser = self.selenium
        url = self.live_server_url + '/story8/'
        browser.get(url)
        assert "Story 8" in browser.title

    def test_search_box_input(self):
        browser = self.selenium
        url = self.live_server_url + '/story8/'
        browser.get(url)

        wait = WebDriverWait(browser, 20)
        wait.until(EC.presence_of_element_located((By.ID, "keyword")))

        searchbox = browser.find_element_by_id('keyword')
        searchbox.send_keys('48 laws of power')

        wait.until(EC.presence_of_element_located((By.XPATH, '//td[text()="The 48 Laws of Power"]')))

        assert "The 48 Laws of Power" in browser.page_source