from django.forms import ModelForm
from .models import Event, Person

class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = '__all__'

class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = '__all__'