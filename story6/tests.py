from django.test import TestCase, Client

from .models import Event, Person

# Create your tests here.

class Story6Index(TestCase):

    def test_url_exists(self):
        response = Client().get('/story6/')
        self.assertEquals(response.status_code, 200)

    def test_template_exists(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_title_and_button_exists(self):
        response = Client().get('/story6/')
        html_response = response.content.decode('utf8')
        self.assertIn("events", html_response)
        self.assertIn("Add Event", html_response)

    def test_model_person_exists(self):
        Person.objects.create(name="Name", nickname="Nickname", domicile="Django Land")
        self.assertEquals(Person.objects.all().count(), 1)

    def test_model_event_exists(self):
        Event.objects.create(name="Name", description="Some event")
        self.assertEquals(Event.objects.all().count(), 1)

    def test_model_and_button_shown_on_page(self):
        person = Person.objects.create(name="Bambang", nickname="Budi", domicile="Django Land")
        event = Event.objects.create(name="Programming", description="Some event")
        event.participant.add(person)
        
        response = Client().get('/story6/')
        html_response = response.content.decode('utf8')
        self.assertIn("Programming", html_response)
        self.assertIn("Budi", html_response)
        self.assertIn("Participants", html_response)
        self.assertIn("Delete", html_response)

class Story6AddDelete(TestCase):

    def test_url_event_exists(self):
        response = Client().get('/story6/addevent/')
        self.assertEquals(response.status_code, 200)

    def test_url_person_exists(self):
        response = Client().get('/story6/addperson/')
        self.assertEquals(response.status_code, 200)

    def test_template_event_exists(self):
        response = Client().get('/story6/addevent/')
        self.assertTemplateUsed(response, 'story6/addevent.html')

    def test_template_person_exists(self):
        response = Client().get('/story6/addperson/')
        self.assertTemplateUsed(response, 'story6/addperson.html')

    def test_form_and_button_event_exists(self):
        response = Client().get('/story6/addevent/')
        html_response = response.content.decode('utf8')
        self.assertIn("Description:", html_response)
        self.assertIn("Add Event", html_response)

    def test_form_and_button_person_exists(self):
        response = Client().get('/story6/addperson/')
        html_response = response.content.decode('utf8')
        self.assertIn("Domicile:", html_response)
        self.assertIn("Add Person", html_response)

    def test_save_database_event(self):
        person = Person.objects.create(name="Name", nickname="Nickname", domicile="Django Land")
        response = Client().post('/story6/addevent/', {'name': 'Belajar', 'description': 'Some event'})
        self.assertEquals(Event.objects.all().count(), 1)

    def test_save_database_person(self):
        response = Client().post('/story6/addperson/', {'name': 'Bambang', 'nickname': 'Budi', 'domicile': 'Django Land'})
        self.assertEquals(Person.objects.all().count(), 1)

    def test_delete_person_working(self):
        person = Person.objects.create(name="Bambang", nickname="Budi", domicile="Django Land")
        event = Event.objects.create(name="Programming", description="Some event")
        event.participant.add(person)
        self.assertEquals(Person.objects.all().count(), 1)
        self.assertEquals(event.participant.all().count(), 1)

        response = Client().get('/story6/deleteperson/' + str(event.id) + '/' + str(person.id))
        self.assertEquals(Person.objects.all().count(), 0)
        self.assertEquals(event.participant.all().count(), 0)

    def test_delete_event_working(self):
        person = Person.objects.create(name="Bambang", nickname="Budi", domicile="Django Land")
        person2 = Person.objects.create(name="Bambangg", nickname="Budi", domicile="Django Land")
        event = Event.objects.create(name="Programming", description="Some event")
        event.participant.add(person, person2)
        self.assertEquals(Event.objects.all().count(), 1)

        response = Client().get('/story6/deleteevent/' + str(event.id))
        self.assertEquals(Event.objects.all().count(), 0)

class Story6EditEvent(TestCase):

    def test_url_exists(self):
        person = Person.objects.create(name="Bambang", nickname="Budi", domicile="Django Land")
        event = Event.objects.create(name="Programming", description="Some event")
        event.participant.add(person)

        response = Client().get('/story6/editparticipant/' + str(event.id))
        self.assertEquals(response.status_code, 200)

    def test_template_exists(self):
        person = Person.objects.create(name="Bambang", nickname="Budi", domicile="Django Land")
        event = Event.objects.create(name="Programming", description="Some event")
        event.participant.add(person)

        response = Client().get('/story6/editparticipant/' + str(event.id))
        self.assertTemplateUsed(response, 'story6/editparticipant.html')

    def test_participant_is_in_event(self):
        person = Person.objects.create(name="Bambang", nickname="Budi", domicile="Django Land")
        event = Event.objects.create(name="Programming", description="Some event")
        event.participant.add(person)

        response = Client().get('/story6/editparticipant/' + str(event.id))
        html_response = response.content.decode('utf8')

        self.assertIn("manage participant", html_response)
        self.assertIn("Remove Participant", html_response)
        self.assertIn("Delete", html_response)

    def test_participant_is_not_in_event(self):
        person = Person.objects.create(name="Bambang", nickname="Budi", domicile="Django Land")
        event = Event.objects.create(name="Programming", description="Some event")

        response = Client().get('/story6/editparticipant/' + str(event.id))
        html_response = response.content.decode('utf8')

        self.assertIn("manage participant", html_response)
        self.assertIn("Add Participant", html_response)
        self.assertIn("Delete", html_response)

    def test_add_participant_to_event(self):
        person = Person.objects.create(name="Bambang", nickname="Budi", domicile="Django Land")
        event = Event.objects.create(name="Programming", description="Some event")

        response = Client().get('/story6/addparticipant/' + str(event.id) + '/' + str(person.id))
        self.assertEquals(event.participant.all().count(), 1)

    def test_remove_participant_from_event(self):
        person = Person.objects.create(name="Bambang", nickname="Budi", domicile="Django Land")
        event = Event.objects.create(name="Programming", description="Some event")
        event.participant.add(person)

        response = Client().get('/story6/removeparticipant/' + str(event.id) + '/' + str(person.id))
        self.assertEquals(event.participant.all().count(), 0)