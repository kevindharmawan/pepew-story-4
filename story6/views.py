from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

from .models import Event, Person
from .forms import EventForm, PersonForm

# Create your views here.
def index(request):
    events = Event.objects.all()
    context = {'events':events}

    return render(request, 'story6/index.html', context)

def addevent(request):
    form = EventForm()
    context = {'form':form}

    if request.method == "POST":
        form = EventForm(request.POST)

        if form.is_valid():
            form.save()

            return redirect('/story6/')

    return render(request, 'story6/addevent.html', context)

def addperson(request):
    form = PersonForm()
    context = {'form':form}

    if request.method == "POST":
        form = PersonForm(request.POST)

        if form.is_valid():
            form.save()

    return render(request, 'story6/addperson.html', context)

def deleteperson(request, event_id, person_id):
    person = Person.objects.get(id=person_id)
    person.delete()

    return redirect('/story6/editparticipant/' + str(event_id))

def deleteevent(request, event_id):
    event = Event.objects.get(id=event_id)
    event.delete()

    return redirect('/story6/')

def editparticipant(request, event_id):
    event = Event.objects.get(id=event_id)
    persons = Person.objects.exclude(id__in=event.participant.all())

    context = {'event':event, 'persons':persons}

    return render(request, 'story6/editparticipant.html', context)

def addparticipant(request, event_id, person_id):
    event = Event.objects.get(id=event_id)
    person = Person.objects.get(id=person_id)
    event.participant.add(person)

    return redirect('/story6/editparticipant/' + str(event_id))

def removeparticipant(request, event_id, person_id):
    event = Event.objects.get(id=event_id)
    person = Person.objects.get(id=person_id)
    event.participant.remove(person)

    return redirect('/story6/editparticipant/' + str(event_id))