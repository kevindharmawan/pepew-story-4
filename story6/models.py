from django.db import models

# Create your models here.

class Person(models.Model):
    name = models.CharField(max_length=100)
    nickname = models.CharField(max_length=100)
    domicile = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Event(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    participant = models.ManyToManyField(Person, blank=True)