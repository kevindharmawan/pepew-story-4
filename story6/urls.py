from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.index, name='index'),
    path('addevent/', views.addevent, name='addevent'),
    path('addperson/', views.addperson, name='addperson'),
    path('deleteevent/<int:event_id>', views.deleteevent, name='deleteevent'),
    path('editparticipant/<int:event_id>', views.editparticipant, name='editparticipant'),
    path('removeparticipant/<int:event_id>/<int:person_id>', views.removeparticipant, name='removeparticipant'),
    path('addparticipant/<int:event_id>/<int:person_id>', views.addparticipant, name='addparticipant'),
    path('deleteperson/<int:event_id>/<int:person_id>', views.deleteperson, name='deleteperson'),
]