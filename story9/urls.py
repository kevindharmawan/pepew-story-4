from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('logout/', views.userlogout, name='logout'),
    path('register/', views.userregister, name='register'),
]