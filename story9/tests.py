from django.test import TestCase, Client
from django.contrib.auth.models import User
from .forms import RegisterForm

# Create your tests here.

class Story8Test(TestCase):

    def test_index_url_exists(self):
        response = Client().get('/story9/')
        self.assertEquals(response.status_code, 200)

    def test_index_template_exists(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9/index.html')

    def test_register_url_exists(self):
        response = Client().get('/story9/register/')
        self.assertEquals(response.status_code, 200)

    def test_register_template_exists(self):
        response = Client().get('/story9/register/')
        self.assertTemplateUsed(response, 'story9/register.html')
    
    def test_register_success(self):
        register_form = {
            'username' : 'kevin',
            'nickname' : 'Kevin',
            'email' : 'kevindharmawan2002@gmail.com',
            'password1' : 'passwordku',
            'password2' : 'passwordku',
        }

        response = Client().post('/story9/register/', register_form)
        self.assertEquals(response.status_code, 200)

        html_response = response.content.decode('utf8')
        self.assertIn("Registration Success!", html_response)
    
    def test_register_failed(self):
        register_form = {
            'username' : 'kevin',
            'nickname' : 'Kevin',
            'email' : 'kevindharmawan2002@gmail.com',
            'password1' : 'passwordku',
            'password2' : 'passwordqoeh',
        }

        response = Client().post('/story9/register/', register_form)
        self.assertEquals(response.status_code, 200)

        html_response = response.content.decode('utf8')
        self.assertIn("Registration Failed", html_response)

    def test_login_content_exists(self):
        response = Client().get('/story9/')

        html_response = response.content.decode('utf8')
        self.assertIn("Username", html_response)
        self.assertIn("Password", html_response)
        self.assertIn("Log In", html_response)
    
    def test_login_success(self):
        register_form = {
            'username' : 'kevin',
            'nickname' : 'Kevin',
            'email' : 'kevindharmawan2002@gmail.com',
            'password1' : 'passwordku',
            'password2' : 'passwordku',
        }

        Client().post('/story9/register/', register_form)

        login_form = {
            'username' : 'kevin',
            'password' : 'passwordku',
        }

        response = Client().post('/story9/', login_form)
        self.assertEquals(response.status_code, 200)

        html_response = response.content.decode('utf8')
        self.assertIn("Welcome", html_response)
        self.assertNotIn("Password", html_response)
    
    def test_login_failed(self):
        register_form = {
            'username' : 'kevin',
            'nickname' : 'Kevin',
            'email' : 'kevindharmawan2002@gmail.com',
            'password1' : 'passwordku',
            'password2' : 'passwordku',
        }

        Client().post('/story9/register/', register_form)

        login_form = {
            'username' : 'kevin',
            'password' : 'passwordqoeh',
        }

        response = Client().post('/story9/', login_form)
        self.assertEquals(response.status_code, 200)

        html_response = response.content.decode('utf8')
        self.assertIn("incorrect", html_response)
        self.assertNotIn("Welcome", html_response)
    
    def test_logout(self):
        register_form = {
            'username' : 'kevin',
            'nickname' : 'Kevin',
            'email' : 'kevindharmawan2002@gmail.com',
            'password1' : 'passwordku',
            'password2' : 'passwordku',
        }

        Client().post('/story9/register/', register_form)

        login_form = {
            'username' : 'kevin',
            'password' : 'passwordku',
        }

        Client().post('/story9/', login_form)
        Client().post('/story9/logout/')

        response = Client().get('/story9/')
        html_response = response.content.decode('utf8')
        self.assertNotIn("Welcome", html_response)
        self.assertIn("Password", html_response)
