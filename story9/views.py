from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.utils.safestring import mark_safe

from .forms import RegisterForm

# Create your views here.

def index(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)

        else :
            messages.error(request,'username or password is incorrect')

    return render(request, 'story9/index.html')

def userregister(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)

        if form.is_valid():
            form.save()
            messages.success(request, 'Registration Success! Please kindly login on the login button (upper right corner)')

        else:
            messages.error(request, mark_safe("Registration Failed"))


    else:
        form = RegisterForm()

    return render(request, 'story9/register.html', {'form':form})

def userlogout(request):
    logout(request)
    return redirect('/story9/')
