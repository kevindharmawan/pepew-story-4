$(document).ready(function(){
  $('.accordion').click(function(event){
    if (!$(event.target).hasClass('up-button') && !$(event.target).hasClass('down-button')) {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).find('.accordion-content').hide(500);
      }
      else {
        $(this).addClass('active');
        $(this).find('.accordion-content').show(500);
      }
    }
  });

  $('.up-button').click(function(){
    $(this).parents('.accordion').insertBefore($(this).parents('.accordion').prev());
  });
    
  $('.down-button').click(function(){
    $(this).parents('.accordion').insertAfter($(this).parents('.accordion').next());
  });
});