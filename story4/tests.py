from django.test import TestCase, Client

from .models import Course

# Create your tests here.

class Story4Test(TestCase):

    def test_index_url_exists(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_index_template_exists(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story4/index.html')

    def test_index_content_exists(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("Kevin", html_response)
        self.assertIn("skill-2.jpg", html_response)

    def test_portfolio_url_exists(self):
        response = Client().get('/portfolio/')
        self.assertEquals(response.status_code, 200)

    def test_portfolio_template_exists(self):
        response = Client().get('/portfolio/')
        self.assertTemplateUsed(response, 'story4/portfolio.html')

    def test_portfolio_content_exists(self):
        response = Client().get('/portfolio/')
        html_response = response.content.decode('utf8')
        self.assertIn("my portfolio", html_response)
        self.assertIn("portfolio-2.png", html_response)

    def test_blog_url_exists(self):
        response = Client().get('/blog/')
        self.assertEquals(response.status_code, 200)

    def test_blog_template_exists(self):
        response = Client().get('/blog/')
        self.assertTemplateUsed(response, 'story4/blog.html')

    def test_blog_content_exists(self):
        response = Client().get('/blog/')
        html_response = response.content.decode('utf8')
        self.assertIn("my story", html_response)

    def test_about_url_exists(self):
        response = Client().get('/about/')
        self.assertEquals(response.status_code, 200)

    def test_about_template_exists(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'story4/about.html')

    def test_about_content_exists(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn("Kevin", html_response)
        self.assertIn("aboutpic.jpg", html_response)

    def test_about_accordion_exists(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn("accordion", html_response)
        self.assertIn("accordion-content", html_response)

    def test_about_accordion_move_feature_exists(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn("up-button", html_response)
        self.assertIn("down-button", html_response)

class Story5Test(TestCase):

    def test_index_url_exists(self):
        response = Client().get('/course/')
        self.assertEquals(response.status_code, 200)

    def test_index_template_exists(self):
        response = Client().get('/course/')
        self.assertTemplateUsed(response, 'story4/course.html')

    def test_index_title_and_button_exists(self):
        response = Client().get('/course/')
        html_response = response.content.decode('utf8')
        self.assertIn("my courses", html_response)
        self.assertIn("Add Course", html_response)

    def test_model_course_exists(self):
        Course.objects.create(
            name="Perancangan & Pemrograman Web",
            initial="PPW",
            lecturer="Pak Gladi",
            credit=3,
            difficulty="HARD",
            description="3 SKS yang memakan 6 SKS",
            year="2020/2021 - ODD",
            room="Online",
            )

        self.assertEquals(Course.objects.all().count(), 1)

    def test_model_and_button_shown_on_page(self):
        Course.objects.create(
            name="Perancangan & Pemrograman Web",
            initial="PPW",
            lecturer="Pak Gladi",
            credit=3,
            difficulty="HARD",
            description="3 SKS yang memakan 6 SKS",
            year="2020/2021 - ODD",
            room="Online",
            )
        
        response = Client().get('/course/')
        html_response = response.content.decode('utf8')
        self.assertIn("PPW", html_response)
        self.assertIn("Detail", html_response)
        self.assertIn("Delete", html_response)

# test for delete course

    def test_delete_url_working(self):
        course = Course.objects.create(
            name="Perancangan & Pemrograman Web",
            initial="PPW",
            lecturer="Pak Gladi",
            credit=3,
            difficulty="HARD",
            description="3 SKS yang memakan 6 SKS",
            year="2020/2021 - ODD",
            room="Online",
            )

        self.assertEquals(Course.objects.all().count(), 1)

        response = Client().get('/deletecourse/' + str(course.id))
        self.assertEquals(Course.objects.all().count(), 0)

# test for course detail

    def test_detail_url_exists(self):
        course = Course.objects.create(
            name="Perancangan & Pemrograman Web",
            initial="PPW",
            lecturer="Pak Gladi",
            credit=3,
            difficulty="HARD",
            description="3 SKS yang memakan 6 SKS",
            year="2020/2021 - ODD",
            room="Online",
            )

        response = Client().get('/detailcourse/' + str(course.id))
        self.assertEquals(response.status_code, 200)

    def test_detail_template_exists(self):
        course = Course.objects.create(
            name="Perancangan & Pemrograman Web",
            initial="PPW",
            lecturer="Pak Gladi",
            credit=3,
            difficulty="HARD",
            description="3 SKS yang memakan 6 SKS",
            year="2020/2021 - ODD",
            room="Online",
            )
            
        response = Client().get('/detailcourse/' + str(course.id))
        self.assertTemplateUsed(response, 'story4/detailcourse.html')

    def test_detail_content_exists(self):
        course = Course.objects.create(
            name="Perancangan & Pemrograman Web",
            initial="PPW",
            lecturer="Pak Gladi",
            credit=3,
            difficulty="HARD",
            description="3 SKS yang memakan 6 SKS",
            year="2020/2021 - ODD",
            room="Online",
            )
            
        response = Client().get('/detailcourse/' + str(course.id))
        html_response = response.content.decode('utf8')
        self.assertIn("Online", html_response)

# test for add course

    def test_add_url_exists(self):
        response = Client().get('/addcourse/')
        self.assertEquals(response.status_code, 200)

    def test_add_template_exists(self):
        response = Client().get('/addcourse/')
        self.assertTemplateUsed(response, 'story4/addcourse.html')

    def test_add_title_and_button_exists(self):
        response = Client().get('/addcourse/')
        html_response = response.content.decode('utf8')
        self.assertIn("add course", html_response)
        self.assertIn("Add Course", html_response)

    def test_save_database_course(self):
        response = Client().post('/addcourse/', {
            'name': "Perancangan & Pemrograman Web",
            'initial': "PPW",
            'lecturer': "Pak Gladi",
            'credit': 3,
            'difficulty': "HARD",
            'description': "3 SKS yang memakan 6 SKS",
            'year': "2020/2021 - ODD",
            'room': "Online",
            })

        self.assertEquals(Course.objects.all().count(), 1)