from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

from .models import Course
from .forms import CourseForm

# Create your views here.
def index(request):
    return render(request, 'story4/index.html')

def portfolio(request):
    return render(request, 'story4/portfolio.html')

def blog(request):
    return render(request, 'story4/blog.html')

def about(request):
    return render(request, 'story4/about.html')

def course(request):
    courses = Course.objects.all()
    context = {'courses':courses}

    return render(request, 'story4/course.html', context)

def detailcourse(request, course_id):
    course = Course.objects.get(id=course_id)
    context = {'course':course}

    return render(request, 'story4/detailcourse.html', context)

def addcourse(request):
    form = CourseForm()
    context = {'form':form}

    if request.method == "POST":
        form = CourseForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('/course/')

    return render(request, 'story4/addcourse.html', context)

def deletecourse(request, course_id):
    course = Course.objects.get(id=course_id)
    course.delete()

    return redirect('/course/')