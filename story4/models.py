from django.db import models

# Create your models here.

class Course(models.Model):
    DIFFICULTY = (
        ('EASY', 'EASY'),
        ('NORMAL', 'NORMAL'),
        ('HARD', 'HARD'),
        ('EXTREME', 'EXTREME'),
        ('INSANE', 'INSANE'),
        )

    YEAR = (
        ('2019/2020 - ODD', '2019/2020 - ODD'),
        ('2019/2020 - EVEN', '2019/2020 - EVEN'),
        ('2020/2021 - ODD', '2020/2021 - ODD'),
        )

    name = models.CharField(max_length=100)
    initial = models.CharField(max_length=10)
    lecturer = models.CharField(max_length=100)
    credit = models.IntegerField()
    difficulty = models.CharField(max_length=10, choices=DIFFICULTY)
    description = models.TextField()
    year = models.CharField(max_length=100, choices=YEAR)
    room = models.CharField(max_length=100)