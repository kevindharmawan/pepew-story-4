from django.urls import path

from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.index, name='index'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('blog/', views.blog, name='blog'),
    path('about/', views.about, name='about'),
    path('course/', views.course, name='course'),
    path('addcourse/', views.addcourse, name='addcourse'),
    path('detailcourse/<int:course_id>', views.detailcourse, name='detailcourse'),
    path('deletecourse/<int:course_id>', views.deletecourse, name='deletecourse'),
]